#include <Wire.h>
 
#define SLAVE_ADDRESS 0x04
int X1Pin = A2; int Y1Pin = A3; int X2Pin = A0; int Y2Pin = A1; // select the 
input pin for the potentiometer int value = 0; // variable to store the value 
coming from the sensor int wat = 5; void setup() {
 
 // initialize i2c as slave
 Wire.begin(SLAVE_ADDRESS);
 
 // define callbacks for i2c communication
// wat = Wire.read();
 Wire.onReceive(setWat); Wire.onRequest(sendData);
}
 
void loop() {
 
}
 
void setWat(int byteCount){ while(Wire.available()){ wat = Wire.read();
 }
}
// callback for sending data
void sendData(){ if (wat == 0){ value = analogRead(X2Pin)/4;
 }
 else if (wat == 1){ value = analogRead(Y2Pin)/4;
 }
 else if (wat == 2){ value = analogRead(X1Pin)/4;
 }
 else if (wat == 3){ value = analogRead(Y1Pin)/4;
 }
 else{ value = 0;
 }
 
 Wire.write(value);
}


import time
import RPi.GPIO as GPIO
import thread
import dbus
import dbus.service
import dbus.mainloop.glib
import time
import smbus

class HID:
    def __init__(self):

        self.key=[
                0xA1, #this is an input report
                0x01, #Usage report = Keyboard
                #Bit array for Modifier keys
                [0, #Right GUI - Windows Key
                 0, #Right ALT
                 0, #Right Shift
                 0, #Right Control
                 0, #Left GUI
                 0, #Left ALT
                 0, #Left Shift
                 0],    #Left Control
                0x00,   #Vendor reserved
                0x00,   #rest is space for 6 keys
                0x00,
                0x00,
                0x00,
                0x00,
                0x00]

        print("Setting up DBus Client")

        self.bus = dbus.SystemBus()
        self.bluetoothservice = self.bus.get_object('org.pijam.gamepad', "/org/pijam/gamepad")
        self.iface = dbus.Interface(self.bluetoothservice, 'org.pijam.gamepad')

        self.event_loop()

    def event_loop(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(37, GPIO.IN,pull_up_down=GPIO.PUD_UP)
        self.key_down = False
        GPIO.add_event_detect(37, GPIO.BOTH, callback=self.button_1, bouncetime=40)
        self.send_input()

    def button_1(self, channel):
        if self.key_down == False:
            self.key_down = True
        else:
            self.key_down = False

    def send_input(self):
        while True:
            time.sleep(0.2)
            if self.key_down == True:
                self.key[4] = 26
            elif self.key_down == False:
                self.key[4] = 0
            bin_str=""
            element=self.key[2]
            for bit in element:
                bin_str += str(bit)
            self.iface.send_keys(int(bin_str,2),self.key[4:10]  )

if __name__ == "__main__":
    HID()
